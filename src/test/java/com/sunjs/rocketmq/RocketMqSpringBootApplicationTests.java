package com.sunjs.rocketmq;

import cn.hutool.core.date.DateUtil;
import com.alibaba.fastjson.JSON;
import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.model.Users;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

/**
 * MQ 消息 测试类
 *
 * @Author: sun
 * @Date: 2021/4/25 3:46 下午
 */
@SpringBootTest
@Slf4j
class RocketMqSpringBootApplicationTests {

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    /**
     * 发送字符串消息测试
     *
     * @Author: sun
     * @Date: 2021/4/25 3:44 下午
     */
    @Test
    void testSendString() {
        SendResult sendResult = rocketMQTemplate.syncSend(MqConfig.Topic.TOPICSTRING, "你好 RocketMQ");
        log.info("MQ发送结果：{}", JSON.toJSONString(sendResult));
    }

    /**
     * 发送Java对象消息测试
     *
     * @Author: sun
     * @Date: 2021/4/25 3:44 下午
     */
    @Test
    void testSendUserModel() {
        Users users = Users.builder().id(1).name("sunjs").registerTime(DateUtil.date()).build();
        SendResult sendResult = rocketMQTemplate.syncSend(MqConfig.Topic.TOPICUSERS, users);
        log.info("MQ发送结果：{}", JSON.toJSONString(sendResult));
    }

    // 2021年04月25日15:54:52  不再使用test测试类进行mq消息的发送测试，转至controller进行操作

}
