package com.sunjs.rocketmq;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {
        "com.sunjs.rocketmq"
})
public class RocketMqSpringBootApplication {

    public static void main(String[] args) {
        SpringApplication.run(RocketMqSpringBootApplication.class, args);
    }

}
