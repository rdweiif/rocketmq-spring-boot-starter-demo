package com.sunjs.rocketmq.consumer.consumemode;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.model.Users;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 测试消息顺序，分两种：并发无序；单线程有序
 * 本示例演示有序
 *
 * @Date 2021/4/25 2:48 下午
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = MqConfig.Topic.TOPICUSERSCONCURRENTLY, //topic
        consumerGroup = MqConfig.GROUP_PREFFIX + MqConfig.Topic.TOPICUSERSCONCURRENTLY, //分组规则，Group-"topic命名"
        //下边可以去掉，都使用的是默认值
        consumeMode = ConsumeMode.CONCURRENTLY, //设置并发接收模式
        messageModel = MessageModel.CLUSTERING, //默认值，集群模式
        selectorType = SelectorType.TAG, // 默认值，标签
        selectorExpression = "*" // 默认值，匹配该topic下所有tag
)
public class UsersConcurrentlyConsumer implements RocketMQListener<Users> {

    /**
     * 直接接收 Users 对象
     * concurrently
     *
     * @Author: sun
     * @Date: 2021/4/25 5:03 下午
     */
    @Override
    public void onMessage(Users body) {
        // 无序，如果测试时输出也是有序的，建议多发送几次，进行观察
        log.info("MQ消费者接收(无序并发)：Topic：{}，用户ID：{}，用户名：{}", MqConfig.Topic.TOPICUSERSCONCURRENTLY, body.getId(), body.getName());
    }

}
