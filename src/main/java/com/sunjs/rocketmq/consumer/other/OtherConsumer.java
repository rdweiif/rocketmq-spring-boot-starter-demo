package com.sunjs.rocketmq.consumer.other;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.consumer._BaseConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Objects;

/**
 * 其他消息语法接收
 *
 * @Date 2021/4/25 5:47 下午
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = MqConfig.Topic.TOPICOTHER, //topic
        consumerGroup = MqConfig.GROUP_PREFFIX + MqConfig.Topic.TOPICOTHER //分组规则，Group-"topic命名"
)
public class OtherConsumer extends _BaseConsumer<String> implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt message) {
        log.info("other消费者接收详情：{}", message);

        // 增加打印key值
        if (!Objects.isNull(message.getKeys())) {
            log.info("other消费者接收-带有自定义KEY值：{}", message.getKeys());
        }

        if (!isVerify(message, String.class)) {
            return;
        }
        // 执行MQ 消息公共处理
        todo();
    }

    /**
     * 具体业务处理
     *
     * @Author: sun
     * @Date: 2021/4/25 3:29 下午
     */
    @Override
    protected void handle() {
        // TOOD 业务处理
        log.info("other消费者业务处理：{}", body);
    }
}
