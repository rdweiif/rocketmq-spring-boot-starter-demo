package com.sunjs.rocketmq.consumer.batch;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.consumer._BaseConsumer;
import com.sunjs.rocketmq.model.Users;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 批量发送消息，消费者
 *
 * @Author: sun
 * @Date: 2021/4/26 7:39 下午
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = MqConfig.Topic.TOPICBATCH, //topic
        consumerGroup = MqConfig.GROUP_PREFFIX + MqConfig.Topic.TOPICBATCH //分组规则，Group-"topic命名"
)
public class BatchConsumer extends _BaseConsumer<Users> implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt message) {
        log.info("批量消息消费者：Topic：{}", message);
        if (!isVerify(message, Users.class)) {
            return;
        }
        // 执行MQ 消息公共处理
        todo();
    }

    /**
     * 具体业务处理
     *
     * @Author: sun
     * @Date: 2021/4/25 3:29 下午
     */
    @Override
    protected void handle() {
        // TOOD 业务处理
    }
}
