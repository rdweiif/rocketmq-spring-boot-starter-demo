package com.sunjs.rocketmq.consumer;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONValidator;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.rocketmq.common.message.MessageExt;

/**
 * 消费者抽象父类
 *
 * @Date 2021/4/25 2:51 下午
 */
@Slf4j
public abstract class _BaseConsumer<T> {

    protected T body;

    /**
     * 校验 RocketMQ Message
     *
     * @Author: sun
     * @Date: 2021/4/23 11:16 上午
     */
    protected boolean isVerify(MessageExt message, Class<T> clazz) {
        // log.info("MQ消费者 - 接收到的数据：{}", message);
        String body = new String(message.getBody());
        if (StringUtils.isBlank(body)) {
            log.info("MQ消息体为空");
            return false;
        }
        // log.info("MQ消费者 - 接收到的消息内容：{}", body);

        JSONValidator a = JSONValidator.from(body);
        if (a.validate()) {
            this.body = JSON.parseObject(body, clazz);
        } else {
            this.body = (T) body;
        }
        return true;
    }

    /**
     * MQ 消息执行公共处理部分
     *
     * @Author: sun
     * @Date: 2021/4/25 3:28 下午
     */
    protected void todo() {
        // 加锁 or 其他处理
        handle();
    }

    /**
     * 抽象钩子算法
     *
     * @Author: sun
     * @Date: 2021/4/25 3:27 下午
     */
    protected abstract void handle();

}
