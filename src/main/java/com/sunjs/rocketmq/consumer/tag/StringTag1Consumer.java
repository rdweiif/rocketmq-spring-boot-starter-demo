package com.sunjs.rocketmq.consumer.tag;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.consumer._BaseConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.ConsumeMode;
import org.apache.rocketmq.spring.annotation.MessageModel;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.annotation.SelectorType;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 测试含tag标签的消息
 *
 * @Date 2021/4/25 5:31 下午
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = MqConfig.Topic.TOPICTAG, //topic
        consumerGroup = MqConfig.GROUP_PREFFIX + MqConfig.Topic.TOPICTAG + "-tag1", //分组规则，Group-"topic命名"
        selectorType = SelectorType.TAG, // 默认值，标签
        selectorExpression = "tag1", // 指定tag标签
        //下边可以去掉，都使用的是默认值
        consumeMode = ConsumeMode.CONCURRENTLY, //并发单线程顺序模式
        messageModel = MessageModel.CLUSTERING //默认值，集群模式
)
public class StringTag1Consumer extends _BaseConsumer<String> implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt message) {
        log.info("tag1-->消费者接收：Topic：{}，Tag：{}", message.getTopic(), message.getTags());
        if (!isVerify(message, String.class)) {
            return;
        }
        // 执行MQ 消息公共处理
        todo();
    }

    /**
     * 具体业务处理
     *
     * @Author: sun
     * @Date: 2021/4/25 3:29 下午
     */
    @Override
    protected void handle() {
        // TOOD 业务处理
        log.info("tag1-->业务处理：消息内容：{}", body);
    }
}
