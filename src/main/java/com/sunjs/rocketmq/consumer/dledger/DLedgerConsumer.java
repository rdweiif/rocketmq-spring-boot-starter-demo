package com.sunjs.rocketmq.consumer.dledger;
/**
 *                     .::::.
 *                   .::::::::.
 *                  :::::::::::    佛主保佑、永无Bug
 *              ..:::::::::::'
 *            '::::::::::::'
 *              .::::::::::
 *         '::::::::::::::..
 *              ..::::::::::::.
 *            ``::::::::::::::::
 *             ::::``:::::::::'        .:::.
 *            ::::'   ':::::'       .::::::::.
 *          .::::'      ::::     .:::::::'::::.
 *         .:::'       :::::  .:::::::::' ':::::.
 *        .::'        :::::.:::::::::'      ':::::.
 *       .::'         ::::::::::::::'         ``::::.
 *   ...:::           ::::::::::::'              ``::.
 *  ```` ':.          ':::::::::'                  ::::..
 *                     '.:::::'                    ':'````..
 */

import com.sunjs.rocketmq.common.MqConfig;
import com.sunjs.rocketmq.consumer._BaseConsumer;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.common.message.MessageExt;
import org.apache.rocketmq.spring.annotation.RocketMQMessageListener;
import org.apache.rocketmq.spring.core.RocketMQListener;
import org.springframework.stereotype.Service;

/**
 * 测试dledger broker自动切换
 *
 * @Author: sun
 * @Date: 2021/4/28 5:27 下午
 */
@Slf4j
@Service
@RocketMQMessageListener(
        topic = MqConfig.Topic.TopicDledger, //topic
        consumerGroup = MqConfig.GROUP_PREFFIX + MqConfig.Topic.TopicDledger //分组规则，Group-"topic命名"
)
public class DLedgerConsumer extends _BaseConsumer<String> implements RocketMQListener<MessageExt> {

    @Override
    public void onMessage(MessageExt message) {
        log.info("《DLeger消费者》消息内容：{}", message);
        if (!isVerify(message, String.class)) {
            return;
        }
        // 执行MQ 消息公共处理
        todo();
    }

    /**
     * 具体业务处理
     *
     * @return void
     * @throws
     * @Date 2021/4/28 5:27 下午
     */
    @Override
    protected void handle() {
        // TOOD 业务处理
    }
}
